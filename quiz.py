from cryptography.fernet import Fernet

KEY = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

MESSAGE = (b'gAAAAABc2cE87CQ8_PZDI1ZbY7BrzFmHB_uqhUmdqOzWLV4rzfBh4pst6OX3phs8Wi0S2uEza8bqg5RTc7LS651qyaczgLprLCVWiBTFT'
           b'nwikrpW7cP5hdvj_lp3oGm2NKqfb9Opweeo7hKU2Y5iy54_kkvNb8BgDcTCoxGG0ZK8R5-df0UDPE0=')


def main():
    f = Fernet(KEY)
    print(f.decrypt(MESSAGE))


if __name__ == "__main__":
    main()
